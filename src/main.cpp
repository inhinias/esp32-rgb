#include <Arduino.h>
#include "BluetoothSerial.h"

#define RED 4
#define GRN 16
#define BLU 17

uint8_t r = 255;
uint8_t g = 255;
uint8_t b = 255;

BluetoothSerial SerialBT;

void setup() {
    // put your setup code here, to run once:
    pinMode(RED, OUTPUT);
    pinMode(GRN, OUTPUT);
    pinMode(BLU, OUTPUT);

    Serial.begin(115200);

    if(!SerialBT.begin("Blufftuff")){
        Serial.println("An error occurred initializing Bluetooth");
        r = 255;
        g = 0;
        b = 0;
    }

    ledcAttachPin(RED, 0);
    ledcAttachPin(GRN, 1);
    ledcAttachPin(BLU, 2);
    ledcSetup(0, 1000, 8);
    ledcSetup(1, 1000, 8);
    ledcSetup(2, 1000, 8);
}

std::vector<char> message;

void loop() {
    while(SerialBT.available()){
        char charMsg = SerialBT.read();
        if(message.size() >=6 ){
            if(message.at(0) == '!' && message.at(1) == 'C'){
                r = message.at(2);
                g = message.at(3);
                b = message.at(4);
            }
            message.clear();
        }
        else{
            message.push_back(charMsg);
        }
    }

    ledcWrite(0, r);
    ledcWrite(1, g);
    ledcWrite(2, b);
}