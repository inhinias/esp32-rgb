# ESP32-RGB

This is a small project containig code to PWM-Control RGB-Strips via Bluetooth serial.
Set the PWM value from 0 to 255 by sending the RGB-Values in the following format:



[‘!’] [‘C’] [byte red] [byte green] [byte blue] [CRC]

This format corresponds to what Adafruit uses in their Bluefruit App.
Therefore their App can be used to controll the strip.